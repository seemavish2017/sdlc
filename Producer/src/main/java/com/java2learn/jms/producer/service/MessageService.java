package com.java2learn.jms.producer.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.java2learn.jms.producer.model.Vendor;
import com.java2learn.jms.producer.sender.MessageSender;

@Component
public class MessageService {
	private static Logger logger = LogManager.getLogger(MessageService.class.getName());
	
	@Autowired
	MessageSender messageSender;
	
	
	public void process(Vendor vendor) {
		// TODO Auto-generated method stub
		logger.info("Inside process method of Message Service");
		logger.info(vendor.toString());
		Gson gson = new Gson();
		String json = gson.toJson(vendor);
		logger.info("message: " + json);
		messageSender.send(json);
		
	}

}
 