package com.java2learn.jms.adapter;

import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.java2learn.jms.listener.ConsumerListener;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	public void sendtoMongo(String json) throws UnknownHostException {
		// TODO Auto-generated method stub
		logger.info("Sending to Mongo DB");
		MongoClient client = new MongoClient();
		DB db = client.getDB("vendor");
		DBCollection collection = db.getCollection("contact");
		logger.info("Converting Json to DBobject");
		DBObject object = (DBObject)JSON.parse(json);
		collection.insert(object);
		logger.info("sent to mongoDB");
		
		
	}

}
