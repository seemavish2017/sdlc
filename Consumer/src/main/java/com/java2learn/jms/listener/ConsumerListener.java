package com.java2learn.jms.listener;

import java.net.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import com.java2learn.jms.adapter.ConsumerAdapter;

public class ConsumerListener implements MessageListener {
	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());  
    @Autowired
	JmsTemplate jmsTemplate;
    @Autowired
    ConsumerAdapter consumerAdapter;
	
    public void onMessage(Message message) {
		// TODO Auto-generated method stub
		logger.info("In onMessage()");
		String json =null;
		if(message instanceof TextMessage)
		{
			try {
				json = ((TextMessage)message).getText();
				logger.info("Sending json to DB" + json);
				consumerAdapter.sendtoMongo(json);
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				logger.error("Message : " +json);
				jmsTemplate.convertAndSend(json);
			}
			catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				logger.error("Message : " +json);
				jmsTemplate.convertAndSend(json);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Message : " +json);
				jmsTemplate.convertAndSend(json);
			}
			
		}
	}

}
